﻿using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics;
using Tri;
using Tri.Win;
using Tri.Win.GameLoop;
using Tri.Win.Rendering;
using Tri.Win.Rendering.Camera;
using Tri.Win.SystemComponent;
using Tri.Win.UI;
using Tri.Win.UI.BitmapFont;
using System.Net;

namespace Colorbreak.GameLoop
{
    public class MainBoardView: GameLoopBase
    {
        public enum Mode
        {
            SingleOrMulti,
            ClientOrServer,
            GetIP,
            SetIP
        };
        private static Texture TitleTexture;
        private static Texture CursorTexture;

        private static BitmapFont BitmapFont;

        OrthographicCamera ocam;

        private static Sprite BackgroundSprite;
        private static Sprite TitleSprite;
        private static Sprite CursorSprite;
        private static Sprite UnderlineSprite;
        private static BitmapLabel FirstBL;
        private static BitmapLabel SecondBL;

        private static int CurrentSelect;
        private static int UnderlinePosition;

        private static string[] IPAddress;
        private static string returnAddress;
        private static int[] AddressSize;
        public Mode mode;

        public MainBoardView() 
            : base("_Selector")
        {
            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport * 0.5f);
            LoadTexture();
        }
        static void LoadTexture()
        {
            TitleTexture = Texture.CreateFromBitmapPath("Resources/title.png");
            CursorTexture = Texture.CreateFromBitmapPath("Resources/cursor.png");
            BitmapFont = BitmapFont.Create("Resources/Gamegirl.fnt");

            BackgroundSprite = new Sprite();
            BackgroundSprite.Texture = Texture.CreateFromBitmapPath("Resources/black.png");
            BackgroundSprite.Size = GameSystem.MainForm.Viewport;
            BackgroundSprite.Anchor = Vector2.Zero; 

            TitleSprite = new Sprite();
            TitleSprite.Anchor = Vector2.Zero;
            TitleSprite.Texture = TitleTexture;
            TitleSprite.Size = TitleTexture.Size * 1.5f;
            TitleSprite.Position = new Vector2(GameSystem.MainForm.Viewport.X / 2 - TitleSprite.Size.X / 2, 350);

            CursorSprite = new Sprite();
            TitleSprite.Anchor = Vector2.Zero;
            CursorSprite.Texture = CursorTexture;
            CursorSprite.Size = CursorTexture.Size;
            CursorSprite.Position = new Vector2(100, 100);

            UnderlineSprite = new Sprite();
            UnderlineSprite.Texture = Texture.CreateFromBitmapPath("Resources/white.png");
            UnderlineSprite.Size = new Vector2(125, 10);
            UnderlineSprite.Anchor = Vector2.Zero;
            UnderlineSprite.IsVisible = false;

            FirstBL = new BitmapLabel()
            {
                FontSize = 64,
                BitmapFont = BitmapFont,
                Position = new Vector2(GameSystem.MainForm.Viewport.X / 2, 260),
                Anchor = new Vector2(0.5f, 0.5f),
                Color = Color4.White,
                Text = "Single Play",
            };
            SecondBL = new BitmapLabel()
            {
                FontSize = 64,
                BitmapFont = BitmapFont,
                Position = new Vector2(GameSystem.MainForm.Viewport.X / 2, 130),
                Anchor = new Vector2(0.5f, 0.5f),
                Color = Color4.White,
                Text = "Multi Play ",
            };
            IPAddress = new string[4];
            for (int i = 0; i < 4; i++)
            {
                IPAddress[i] = "   ";
            }
            AddressSize = new int[4] { 0, 0, 0, 0 };
            CurrentSelect = 0;
            CursorSprite.Position = new Vector2(GameSystem.MainForm.Viewport.X / 2 - FirstBL.Text.Length * 32 - 50, FirstBL.Position.Y - 5);

            returnAddress = "Invalid";

            UnderlinePosition = 0;
        }

        private void UpdateCursorPosition()
        {
            CursorSprite.IsVisible = true;
            UnderlineSprite.IsVisible = false;
            if (CurrentSelect == 0)
            {
                CursorSprite.Position = new Vector2(GameSystem.MainForm.Viewport.X / 2 - FirstBL.Text.Length * 32 - 50, FirstBL.Position.Y - 5);
                if (mode == Mode.SetIP)
                {
                    CursorSprite.IsVisible = false;
                    UnderlineSprite.IsVisible = true;
                    UnderlineSprite.Position = new Vector2(407 + 171 * UnderlinePosition, 220);
                }

            }
            else
                CursorSprite.Position = new Vector2(GameSystem.MainForm.Viewport.X / 2 - SecondBL.Text.Length * 32 - 50, SecondBL.Position.Y - 5);
        }

        private void UpdateMenu()
        {
            CurrentSelect = 0;
            FirstBL.FontSize = 64;

            switch (mode)
            {
                case Mode.SingleOrMulti:
                    FirstBL.Text = "Single Play";
                    SecondBL.Text = "Multi Play ";
                    break;

                case Mode.ClientOrServer:
                    FirstBL.Text = "Client";
                    SecondBL.Text = "Server";
                    break;

                case Mode.GetIP:
                    IPHostEntry host = Dns.GetHostByName(Dns.GetHostName());
                    FirstBL.Text = "Your IP: " + host.AddressList[0];
                    SecondBL.Text = "Start";

                    FirstBL.FontSize = 48;
                    CurrentSelect = 1;
                    break;

                case Mode.SetIP:
                    FirstBL.Text = "IP: " + IPAddress[0] + "." + IPAddress[1] + "." + IPAddress[2] + "." + IPAddress[3];
                    SecondBL.Text = "Start";

                    FirstBL.FontSize = 48;
                    UnderlinePosition = 0;
                    break;

                default:
                    break;
            }
            UpdateCursorPosition();
        }

        private void MakeReturnAddress()
        {
            if (AddressSize[0] > 0 && AddressSize[1] > 0 && AddressSize[2] > 0 && AddressSize[3] > 0)
            {
                returnAddress = IPAddress[0].Substring(3 - AddressSize[0], AddressSize[0]) + "."
                    + IPAddress[1].Substring(3 - AddressSize[1], AddressSize[1]) + "."
                    + IPAddress[2].Substring(3 - AddressSize[2], AddressSize[2]) + "."
                    + IPAddress[3].Substring(3 - AddressSize[3], AddressSize[3]);
            }
            else
                returnAddress = "Invalid";
        }

        public override void Update(Second deltaTime)
        {
            #region Up & Down & Left & Right
            if ((InputHelper.IsPressed(Key.Up) || InputHelper.IsPressed(Key.Down)) && mode != Mode.GetIP)
            { 
                CurrentSelect = 1 - CurrentSelect;
                UpdateCursorPosition();
            }
            else if (InputHelper.IsPressed(Key.Left) && mode == Mode.SetIP && UnderlinePosition > 0)
            {
                UnderlinePosition--;
                UpdateCursorPosition();
            }
            else if (InputHelper.IsPressed(Key.Right) && mode == Mode.SetIP && UnderlinePosition < 3)
            {
                UnderlinePosition++;
                UpdateCursorPosition();
            }
            #endregion
            #region Enter
            else if (InputHelper.IsPressed(Key.Enter))
            {
                switch(mode)
                {
                    case Mode.SingleOrMulti:
                        if (CurrentSelect == 0)
                            GameSystem.RunGameLoop(new SingleGameLoop());
                        else
                        {
                            mode = Mode.ClientOrServer;
                            UpdateMenu();
                        }
                        break;

                    case Mode.ClientOrServer:
                        if (CurrentSelect == 0)
                        {
                            mode = Mode.SetIP;
                            UpdateMenu();
                        }
                        else
                        {
                            mode = Mode.GetIP;
                            UpdateMenu();
                        }
                        break;

                    case Mode.GetIP:
                        GameSystem.RunGameLoop(new ServerGameLoop());
                        break;

                    case Mode.SetIP:
                        if (CurrentSelect == 1)
                            GameSystem.RunGameLoop(new ClientGameLoop(returnAddress));
                        break;

                    default:
                        break;
                }
            }
            #endregion
            #region ESC
            else if (InputHelper.IsPressed(Key.Escape))
            {
                switch (mode)
                {
                    case Mode.ClientOrServer:
                        mode = Mode.SingleOrMulti;
                        UpdateMenu();
                        break;

                    case Mode.GetIP:
                    case Mode.SetIP:
                        mode = Mode.ClientOrServer;
                        UpdateMenu();
                        break;

                    default:
                        break;
                }
            }
            #endregion
            #region Backspace
            else if (InputHelper.IsPressed(Key.BackSpace))
            {
                if(mode == Mode.SetIP && AddressSize[UnderlinePosition] > 0)
                {
                    IPAddress[UnderlinePosition] = " " + IPAddress[UnderlinePosition];
                    //IPAddress[UnderlinePosition].Remove(IPAddress.Length - 1);
                    IPAddress[UnderlinePosition] = IPAddress[UnderlinePosition].Substring(0, 3);
                    AddressSize[UnderlinePosition]--;
                    FirstBL.Text = "IP: " + IPAddress[0] + "." + IPAddress[1] + "." + IPAddress[2] + "." + IPAddress[3];
                    MakeReturnAddress();
                }
            }
            #endregion
            #region Number
            for (int i = 0; i<10; i++)
            {
                if(InputHelper.IsPressed(Key.Number0 + i))
                {
                    if (mode == Mode.SetIP && AddressSize[UnderlinePosition] < 3)
                    {
                        IPAddress[UnderlinePosition] = IPAddress[UnderlinePosition] + i;
                        IPAddress[UnderlinePosition] = IPAddress[UnderlinePosition].Substring(1, 3);
                        AddressSize[UnderlinePosition]++;
                        FirstBL.Text = "IP: " + IPAddress[0] + "." + IPAddress[1] + "." + IPAddress[2] + "." + IPAddress[3];
                        MakeReturnAddress();
                    }
                }
            }
            #endregion
        }

        public override void Render(Second deltaTime) 
        {
            BackgroundSprite.Render(ref ocam.Matrix);
            TitleSprite.Render(ref ocam.Matrix);
            
            CursorSprite.Render(ref ocam.Matrix);

            UnderlineSprite.Render(ref ocam.Matrix);

            FirstBL.Render(ref ocam.Matrix);
            SecondBL.Render(ref ocam.Matrix);
        }
    }
}

