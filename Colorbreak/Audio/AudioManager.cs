﻿using System.Collections.Generic;
using Tri.Win.Sound;

namespace Colorbreak.Audio
{
    public enum AudioEnum
    {
        Title,
        InGame,
    }

    public static class AudioManager
    {
        static Dictionary<AudioEnum, SoundContext> soundContextDic;
        static AudioManager()
        {
            soundContextDic = new Dictionary<AudioEnum, SoundContext>();

            soundContextDic.Add(AudioEnum.InGame, SoundContext.Create("Resources/InGame.wav"));
            soundContextDic[AudioEnum.InGame].SetPlayback(true);
        }

        public static void Start(AudioEnum audioToPlay)
        {
            Stop();
            soundContextDic[audioToPlay].Play();
        }
        public static void Stop()
        {
            foreach (SoundContext sc in soundContextDic.Values)
                sc.Stop();
        }
    }
}
