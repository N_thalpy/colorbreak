﻿using OpenTK;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using Tri;
using Tri.Win.Rendering;
using Tri.Win.SystemComponent;
using Tri.Win.UI;

namespace Colorbreak.GameObject
{
    [Flags]
    public enum Block
    {
        None = 0,

        Void = 1 << 0,
        R = (1 << 1) + Void,
        G = (1 << 2) + Void,
        B = (1 << 3) + Void,
        RG = R + G - Void,
        RB = R + B - Void,
        GB = G + B - Void,
        RGB = R + G + B - 2 * Void,
    };

    public class GameBoard
    {
        private Second Cooltime;
        private static Second InitialCooltime;
        private static Second AnimationCoolTime;

        public int Width;
        public int Height;

        public int Score;
        public int BrokenBlockCount = 0;
        public int ChainCount;
        public bool GameOvered;

        private GameBoardView view;
        private FallingBlock fallingBlock;

        /// <summary>
        /// Access BoardData[x, y]
        /// ^ y
        /// |
        /// | origin x
        /// o-------->
        /// </summary>
        private Block[,] BoardData;

        private Second falldownTimer;
        private Second animationTimer;

        static GameBoard()
        {
            InitialCooltime = (Second)0.7f;
            AnimationCoolTime = (Second)0.1f;
        }
        public GameBoard(int width, int height, Vector2 pos)
        {
            Width = width;
            Height = height;
            ChainCount = 0;
            Cooltime = (Second)0.7f;

            BoardData = new Block[width, height];
            view = new GameBoardView(width, height);
            view.Position = pos;

            RefreshFallingBlock();
        }
        
        public void Update(Second deltaTime)
        {
            if (GameOvered == true)
                return;

            bool isChainIncreased = false;
            bool isAnimating = false;
            #region Check Unstable Blocks
            for (int x = 0; x < Width; x++)
                for (int y = 1; y < Height; y++)
                    if (BoardData[x, y] != Block.None && BoardData[x, y - 1] == Block.None)
                        isAnimating = true;
            #endregion
            #region Animation
            if (isAnimating == true)
            {
                animationTimer += deltaTime;
                while (animationTimer > AnimationCoolTime)
                {
                    animationTimer -= AnimationCoolTime;
                    for (int x = 0; x < Width; x++)
                        for (int y = 1; y < Height; y++)
                            if (BoardData[x, y] != Block.None && BoardData[x, y - 1] == Block.None)
                            {
                                BoardData[x, y - 1] = BoardData[x, y];
                                BoardData[x, y] = Block.None;
                            }
                }
            }
            #endregion

            #region Refresh Block
            if (fallingBlock == null && isAnimating == false)
                RefreshFallingBlock();
            #endregion

            #region Block Movement
            if (isAnimating == false)
            {
                if (InputHelper.IsPressed(Key.Left))
                {
                    bool flag = true;
                    for (int i = 0; i < 3; i++)
                        if ((int)fallingBlock.GetPosition(i).X == 0 ||
                            BoardData[(int)fallingBlock.GetPosition(i).X - 1, (int)fallingBlock.GetPosition(i).Y] != Block.None)
                            flag = false;

                    if (flag == true)
                        fallingBlock.X -= 1;

                }
                if (InputHelper.IsPressed(Key.Right))
                {
                    bool flag = true;
                    for (int i = 0; i < 3; i++)
                        if ((int)fallingBlock.GetPosition(i).X == Width - 1 ||
                            BoardData[(int)fallingBlock.GetPosition(i).X + 1, (int)fallingBlock.GetPosition(i).Y] != Block.None)
                            flag = false;

                    if (flag == true)
                        fallingBlock.X += 1;
                }
                if (InputHelper.IsPressed(Key.Down))
                {
                    bool flag = true;
                    for (int i = 0; i < 3; i++)
                        if ((int)fallingBlock.GetPosition(i).Y == 0 ||
                            BoardData[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y - 1] != Block.None)
                            flag = false;

                    if (flag == true)
                    {
                        fallingBlock.Y -= 1;
                        falldownTimer = Second.Zero;
                    }
                    else
                    {
                        falldownTimer = Cooltime - deltaTime;
                    }
                }
                if (InputHelper.IsPressed(Key.A))
                {
                    for (int i = 0; i < 3; i++)
                        if (((fallingBlock.BlockData[i]) & (Block.R)) == (Block.R))
                        {
                            fallingBlock.BlockData[i] = fallingBlock.BlockData[i] - Block.R + Block.Void;
                        }
                }
                if (InputHelper.IsPressed(Key.S))
                {
                    for (int i = 0; i < 3; i++)
                        if (((fallingBlock.BlockData[i]) & (Block.G)) == (Block.G))
                        {
                            fallingBlock.BlockData[i] = fallingBlock.BlockData[i] - Block.G + Block.Void;
                        }
                }
                if (InputHelper.IsPressed(Key.D))
                {
                    for (int i = 0; i < 3; i++)
                        if (((fallingBlock.BlockData[i]) & (Block.B)) == (Block.B))
                        {
                            fallingBlock.BlockData[i] = fallingBlock.BlockData[i] - Block.B + Block.Void;
                        }
                }
                #region __TEMP__ Block Spin __TEMP__
                if (InputHelper.IsPressed(Key.Z))
                {
                    fallingBlock.Dir = (Direction)(((int)fallingBlock.Dir - 1 - 1 + 4) % 4 + 1);

                    bool flag = true;
                    int dXValue = 0;

                    for (int i = 0; i < 3; i++)
                        if ((int)fallingBlock.GetPosition(i).Y < Height && (int)fallingBlock.GetPosition(i).Y >= 0)
                        {
                            if ((int)fallingBlock.GetPosition(i).X >= 0 &&
                                    ((int)fallingBlock.GetPosition(i).X >= Width ||
                                    BoardData[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y] != Block.None)
                               )
                            {
                                bool flag2 = true;
                                for (int j = 0; j < 3; j++)
                                    if ((int)fallingBlock.GetPosition(j).X <= 0 ||
                                        BoardData[(int)fallingBlock.GetPosition(j).X - 1, (int)fallingBlock.GetPosition(j).Y] != Block.None)
                                        flag2 = false;

                                if (flag2 == true)
                                {
                                    fallingBlock.X -= 1;
                                    dXValue = -1;
                                }
                            }

                            if ((int)fallingBlock.GetPosition(i).X < Width &&
                                        ((int)fallingBlock.GetPosition(i).X <= -1 ||
                                        BoardData[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y] != Block.None)
                                    )
                            {
                                bool flag2 = true;
                                for (int j = 0; j < 3; j++)
                                    if ((int)fallingBlock.GetPosition(j).X >= Width - 1 ||
                                        BoardData[(int)fallingBlock.GetPosition(j).X + 1, (int)fallingBlock.GetPosition(j).Y] != Block.None)
                                        flag2 = false;

                                if (flag2 == true)
                                {
                                    fallingBlock.X += 1;
                                    dXValue = 1;
                                }
                            }
                        }


                    for (int i = 0; i < 3; i++)
                        if ((int)fallingBlock.GetPosition(i).X >= Width || (int)fallingBlock.GetPosition(i).X <= -1 ||
                            (int)fallingBlock.GetPosition(i).Y >= Height || (int)fallingBlock.GetPosition(i).Y <= -1 ||
                            BoardData[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y] != Block.None)
                            flag = false;

                    if (flag == false)
                    { 
                        fallingBlock.Dir = (Direction)(((int)fallingBlock.Dir - 1 + 1) % 4 + 1);
                        fallingBlock.X -= dXValue;
                    }
                }
                if (InputHelper.IsPressed(Key.X))
                {
                    fallingBlock.Dir = (Direction)(((int)fallingBlock.Dir - 1 + 1) % 4 + 1);

                    bool flag = true;
                    int dXValue = 0;

                    for (int i = 0; i < 3; i++)
                        if ((int)fallingBlock.GetPosition(i).Y < Height && (int)fallingBlock.GetPosition(i).Y >= 0)
                        {
                            if ((int)fallingBlock.GetPosition(i).X >= 0 &&
                                    ((int)fallingBlock.GetPosition(i).X >= Width ||
                                    BoardData[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y] != Block.None)
                               )
                            {
                                bool flag2 = true;
                                for (int j = 0; j < 3; j++)
                                    if ((int)fallingBlock.GetPosition(j).X <= 0 ||
                                        BoardData[(int)fallingBlock.GetPosition(j).X - 1, (int)fallingBlock.GetPosition(j).Y] != Block.None)
                                        flag2 = false;

                                if (flag2 == true)
                                {
                                    fallingBlock.X -= 1;
                                    dXValue = -1;
                                }
                            }

                            if ((int)fallingBlock.GetPosition(i).X < Width &&
                                        ((int)fallingBlock.GetPosition(i).X <= -1 ||
                                        BoardData[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y] != Block.None)
                                    )
                            {
                                bool flag2 = true;
                                for (int j = 0; j < 3; j++)
                                    if ((int)fallingBlock.GetPosition(j).X >= Width - 1 ||
                                        BoardData[(int)fallingBlock.GetPosition(j).X + 1, (int)fallingBlock.GetPosition(j).Y] != Block.None)
                                        flag2 = false;

                                if (flag2 == true)
                                {
                                    fallingBlock.X += 1;
                                    dXValue = 1;
                                }
                            }
                        }

                    for (int i = 0; i < 3; i++)
                        if ((int)fallingBlock.GetPosition(i).X >= Width || (int)fallingBlock.GetPosition(i).X <= -1 ||
                            (int)fallingBlock.GetPosition(i).Y >= Height || (int)fallingBlock.GetPosition(i).Y <= -1 ||
                            BoardData[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y] != Block.None)
                            flag = false;

                    if (flag == false)
                    {
                        fallingBlock.Dir = (Direction)(((int)fallingBlock.Dir - 1 - 1 + 4) % 4 + 1);
                        fallingBlock.X -= dXValue;
                    }
                }
                #endregion
            }
            #endregion
            #region Block Fall / Stack
            if (isAnimating == false)
            {
                falldownTimer += deltaTime;
                while (falldownTimer >= Cooltime)
                {
                    falldownTimer -= Cooltime;

                    bool flag = false;
                    for (int i = 0; i < 3; i++)
                    {
                        if (fallingBlock.GetPosition(i).Y == 0)
                            flag = true;
                        else if (BoardData[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y - 1] != Block.None)
                            flag = true;
                    }
                    if (flag)
                    {
                        for (int i = 0; i < 3; i++)
                            BoardData[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y] = fallingBlock.BlockData[i];
                        fallingBlock = null;
                        break;
                    }

                    fallingBlock.Y -= 1;
                }
            }
            #endregion

            bool isUnstable = false;
            #region Check Unstable Blocks
            for (int x = 0; x < Width; x++)
                for (int y = 1; y < Height; y++)
                    if (BoardData[x, y] != Block.None && BoardData[x, y - 1] == Block.None)
                        isUnstable = true;
            #endregion

            #region Block Match Check
            if (isAnimating == false && isUnstable == false)
            {
                int[] dx = new int[4] { 1, -1, 0, 0 };
                int[] dy = new int[4] { 0, 0, 1, -1 };

                List<Vector2> connectedList = new List<Vector2>();
                Queue<Vector2> queue = new Queue<Vector2>();
                Block currentColor;
                for (int x = 0; x < Width; x++)
                    for (int y = 0; y < Height; y++)
                    {
                        if (BoardData[x, y] == Block.None || BoardData[x, y] == Block.Void)
                            continue;

                        connectedList.Clear();
                        queue.Clear();
                        queue.Enqueue(new Vector2(x, y));
                        currentColor = BoardData[x, y];

                        while (queue.Count != 0)
                        {
                            int px = (int)queue.Peek()[0];
                            int py = (int)queue.Peek()[1];
                            connectedList.Add(new Vector2(px, py));
                            queue.Dequeue();

                            for (int i = 0; i < 4; i++)
                                if (0 <= px + dx[i] && px + dx[i] < Width &&
                                    0 <= py + dy[i] && py + dy[i] < Height &&
                                    BoardData[px + dx[i], py + dy[i]] == currentColor &&
                                    connectedList.Contains(new Vector2(px + dx[i], py + dy[i])) == false)
                                    queue.Enqueue(new Vector2(px + dx[i], py + dy[i]));
                        }

                        if (connectedList.Count >= 4)
                            foreach (Vector2 vec in connectedList)
                            {
                                int px = (int)vec[0];
                                int py = (int)vec[1];
                                BoardData[px, py] = Block.None;

                                if (isChainIncreased == false)
                                {
                                    isChainIncreased = true;
                                    ChainCount = ChainCount + 1;
                                }
                                Score += ChainCount;
                                BrokenBlockCount = BrokenBlockCount + 1;
                                Cooltime = (Second)(((double)InitialCooltime) * (Math.Pow(2, -BrokenBlockCount / 30.0f) * 0.66f + 0.34f)); //magic number

                                /*
                                for (int i = 0; i < 4; i++)
                                    if (0 <= px + dx[i] && px + dx[i] < Width &&
                                        0 <= py + dy[i] && py + dy[i] < Height &&
                                        BoardData[px + dx[i], py + dy[i]] == Block.Void)
                                        BoardData[px + dx[i], py + dy[i]] = Block.None;
                                */
                            }
                    }
            }
            #endregion

            #region GameOver Check
            for (int x = 0; x < Width; x++)
                for (int y = Height - 2; y < Height; y++)
                    if (BoardData[x, y] != Block.None)
                        GameOvered = true;
            #endregion
        }
        public void Render(ref Matrix4 mat)
        {
            Block[,] cloned = (Block[,])BoardData.Clone();
            if (fallingBlock != null)
                for (int i = 0; i < 3; i++)
                    cloned[(int)fallingBlock.GetPosition(i).X, (int)fallingBlock.GetPosition(i).Y] = fallingBlock.BlockData[i];
    
            view.SetBoardData(cloned, Score);
            view.Render(ref mat);
        }

        public Block[,] GetViewData()
        {
            return view.BoardData;
        }
        private void RefreshFallingBlock()
        {
            fallingBlock = FallingBlock.RandomGenerate();
            fallingBlock.X = Width / 2;
            fallingBlock.Y = Height - 1;
            ChainCount = 0;
        }
    }
}
