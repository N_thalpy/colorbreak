﻿using Colorbreak.GameObject;
using Colorbreak.Network.Packet;
using System;
using System.Text;

namespace Colorbreak.Network
{
    public static class PacketSerializer
    {
        public static String Serialize(PacketBase pb)
        {
            if (pb is SyncPacket)
            {
                SyncPacket sp = pb as SyncPacket;
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("SYNC");
                sb.Append(String.Format("{0}\n{1}\n", sp.BlockData.GetLength(0), sp.BlockData.GetLength(1)));
                for (int x = 0; x < sp.BlockData.GetLength(0); x++)
                    for (int y = 0; y < sp.BlockData.GetLength(1); y++)
                        if (sp.BlockData[x, y] == Block.None)
                            sb.Append("0");
                        else
                            sb.Append(1 + (((int)sp.BlockData[x, y] - 1) >> 1));
                sb.Append("\n");
                sb.Append(sp.Score);
                sb.Append("\n");
                sb.Append(sp.IsGameOver);

                return sb.ToString();
            }
            else
                throw new ArgumentException();
        }
        public static PacketBase Deserialize(String s)
        {
            String[] tokenized = s.Split('\n');
            if (tokenized[0].Trim() == "SYNC")
            {
                SyncPacket sp = new SyncPacket();
                sp.BlockData = new Block[Int32.Parse(tokenized[1]), Int32.Parse(tokenized[2])];
                for (int x = 0; x < Int32.Parse(tokenized[1]); x++)
                    for (int y = 0; y < Int32.Parse(tokenized[2]); y++)
                    {
                        int value = Int32.Parse(tokenized[3][x * Int32.Parse(tokenized[2]) + y].ToString());
                        if (value == 0)
                            sp.BlockData[x, y] = Block.None;
                        else
                            sp.BlockData[x, y] = (Block)(((value - 1) << 1) + 1);
                    }
                sp.Score = Int32.Parse(tokenized[4]);
                sp.IsGameOver = Boolean.Parse(tokenized[5]);

                return sp;
            }
            else
                throw new ArgumentException();
        }
    }
}
