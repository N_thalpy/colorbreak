﻿using Colorbreak.Audio;
using Colorbreak.GameObject;
using OpenTK;
using OpenTK.Input;
using System.Drawing;
using Tri;
using Tri.Win;
using Tri.Win.GameLoop;
using Tri.Win.Rendering;
using Tri.Win.Rendering.Camera;
using Tri.Win.SystemComponent;
using Tri.Win.UI;


namespace Colorbreak.GameLoop
{
    public class SingleGameLoop : GameLoopBase
    {
        OrthographicCamera ocam;
        GameBoard board;

        private Sprite BackgroundSprite;
        private Sprite DescriptionSprite;

        Sprite sp;

        public SingleGameLoop()
            :base("_TestGameLoop")
        {
            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport * 0.5f);
            board = new GameBoard(6, 12, GameSystem.MainForm.Viewport * 0.5f);

            BackgroundSprite = new Sprite();
            BackgroundSprite.Texture = Texture.CreateFromBitmapPath("Resources/black.png");
            BackgroundSprite.Size = GameSystem.MainForm.Viewport;
            BackgroundSprite.Anchor = Vector2.Zero;

            Texture desTex = Texture.CreateFromBitmapPath("Resources/game_description.png");
            DescriptionSprite = new Sprite();
            DescriptionSprite.Texture = desTex;
            DescriptionSprite.Position = new Vector2(GameSystem.MainForm.Viewport.X * 0.25f, GameSystem.MainForm.Viewport.Y * 0.5f);
            DescriptionSprite.Size = desTex.Size;
            DescriptionSprite.Anchor = Vector2.One * 0.5f;

            Texture tex = Texture.CreateFromBitmapPath("Resources/game_over.png");
            sp = new Sprite()
            {
                Position = GameSystem.MainForm.Viewport * 0.5f,
                Anchor = Vector2.One * 0.5f,
                Color = Color.White,
                Texture = tex,
                Size = tex.Size,
            };

            AudioManager.Start(AudioEnum.InGame);
        }

        public override void Update(Second deltaTime)
        {
            board.Update(deltaTime);
            if (InputHelper.IsPressed(Key.Escape) && board.GameOvered == true)
            {
                GameSystem.RunGameLoop(new MainBoardView());
            }
        }

        public override void Render(Second deltaTime)
        {
            BackgroundSprite.Render(ref ocam.Matrix);
            DescriptionSprite.Render(ref ocam.Matrix);
            board.Render(ref ocam.Matrix);
            if (board.GameOvered == true)
            {
                sp.Render(ref ocam.Matrix);
            }
        }
    }
}

