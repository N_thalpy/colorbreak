﻿using System;
using Colorbreak.GameLoop;
using Colorbreak.GameObject;
using Tri.Win;

namespace Colorbreak
{
    public class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            GameSystem.IsDebug = true;
            GameSystem.Initialize(1280, 760, "Colorbreak");
            
            GameSystem.RunGameLoop(new MainBoardView());
            GameSystem.Run();
            GameSystem.Dispose();
        }
    }
}
