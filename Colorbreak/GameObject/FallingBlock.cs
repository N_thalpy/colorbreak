﻿using OpenTK;
using System;

namespace Colorbreak.GameObject
{
    public enum Direction
    {
        None,

        Left,
        Up,
        Right,
        Down,
    }

    public class FallingBlock
    {
        private static Random rd;

        public int X;
        public int Y;
        public Direction Dir;
        public Block[] BlockData;

        static FallingBlock()
        {
            rd = new Random();
        }
        private FallingBlock()
        {
        }

        public Vector2 GetPosition(int idx)
        {
            Vector2 pos = new Vector2(X, Y);
            if (Dir == Direction.Down)
                pos += Vector2.UnitY * 2;
            if (Dir == Direction.Left)
                pos += Vector2.UnitX;
            if (Dir == Direction.Right)
                pos -= Vector2.UnitX;

            for (int i = 0; i < idx; i++)
                switch (Dir)
                {
                    case Direction.Down:
                        pos -= Vector2.UnitY;
                        break;

                    case Direction.Left:
                        pos -= Vector2.UnitX;
                        break;

                    case Direction.Right:
                        pos += Vector2.UnitX;
                        break;

                    case Direction.Up:
                        pos += Vector2.UnitY;
                        break;
                }

            return pos;
        }

        public static FallingBlock RandomGenerate()
        {
            // Dark magic
            Block a = (Block)((rd.Next(1, 8) << 1) + 1);
            Block b = (Block)((rd.Next(1, 8) << 1) + 1);
            Block c = (Block)((rd.Next(1, 8) << 1) + 1);

            return new FallingBlock()
            {
                Dir = Direction.Right,
                BlockData = new Block[3] { a, b, c },
            };
        }
    }
}
