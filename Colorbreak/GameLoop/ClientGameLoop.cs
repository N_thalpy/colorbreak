﻿using Colorbreak.Audio;
using Colorbreak.GameObject;
using Colorbreak.Network;
using Colorbreak.Network.Packet;
using Lidgren.Network;
using OpenTK;
using OpenTK.Input;
using System;
using Tri;
using Tri.Win;
using Tri.Win.GameLoop;
using Tri.Win.Rendering.Camera;
using Tri.Win.UI;
using Tri.Win.Rendering;
using Tri.Win.SystemComponent;
using System.Drawing;
using Tri.Win.UI.BitmapFont;

namespace Colorbreak.GameLoop
{
    public class ClientGameLoop : GameLoopBase
    {
        int port = 25252;
        String ip = "143.248.233.44";
        NetClient client;

        OrthographicCamera ocam;
        GameBoard board;
        GameBoardView boardView;

        private Sprite BackgroundSprite;
        private Sprite DescriptionSprite;
        private Sprite PlayerGameOverSprite;
        private Sprite EnemyGameOverSprite;

        private bool EnemyGameOvered;
        private BitmapLabel WinLoseLabel;

        public ClientGameLoop(string IPAddress)
            : base("_MultiTest")
        {
            GameSystem.LogSystem.WriteLine("Client Started");
            if (IPAddress != "Invalid") ip = IPAddress;
            NetPeerConfiguration config = new NetPeerConfiguration("Colorbreak;");
            config.AutoFlushSendQueue = false;

            client = new NetClient(config);
            client.Start();

            client.Connect(ip, port);

            BackgroundSprite = new Sprite();
            BackgroundSprite.Texture = Texture.CreateFromBitmapPath("Resources/black.png");
            BackgroundSprite.Size = GameSystem.MainForm.Viewport;
            BackgroundSprite.Anchor = Vector2.Zero;

            Texture desTex = Texture.CreateFromBitmapPath("Resources/game_description.png");
            DescriptionSprite = new Sprite();
            DescriptionSprite.Texture = desTex;
            DescriptionSprite.Size = desTex.Size;
            DescriptionSprite.Position = GameSystem.MainForm.Viewport * 0.5f;
            DescriptionSprite.Anchor = Vector2.One * 0.5f;

            Texture gameOvertex = Texture.CreateFromBitmapPath("Resources/game_over.png");

            PlayerGameOverSprite = new Sprite()
            {
                Position = new Vector2(GameSystem.MainForm.Viewport.X * 0.25f, GameSystem.MainForm.Viewport.Y * 0.5f),
                Anchor = Vector2.One * 0.5f,
                Color = Color.White,
                Texture = gameOvertex,
                Size = gameOvertex.Size,
            };

            EnemyGameOverSprite = new Sprite()
            {
                Position = new Vector2(GameSystem.MainForm.Viewport.X * 0.75f, GameSystem.MainForm.Viewport.Y * 0.5f),
                Anchor = Vector2.One * 0.5f,
                Color = Color.White,
                Texture = gameOvertex,
                Size = gameOvertex.Size,
            };

            EnemyGameOvered = false;

            ocam = new OrthographicCamera(GameSystem.MainForm.Viewport, GameSystem.MainForm.Viewport * 0.5f);
            board = new GameBoard(6, 12, new Vector2(GameSystem.MainForm.Viewport.X * 0.25f, GameSystem.MainForm.Viewport.Y * 0.5f));
            boardView = new GameBoardView(6, 12);

            boardView.Position = new Vector2(GameSystem.MainForm.Viewport.X * 0.75f, GameSystem.MainForm.Viewport.Y * 0.5f);

            WinLoseLabel = new BitmapLabel()
            {
                BitmapFont = BitmapFont.Create("Resources/Gamegirl.fnt"),
                FontSize = 72,
                Text = String.Empty,
                Color = Color.White,
                Position = GameSystem.MainForm.Viewport * 0.5f,
                Anchor = new Vector2(0.5f, 0.5f),
            };

            AudioManager.Start(AudioEnum.InGame);
        }

        public override void Update(Second deltaTime)
        {
            do
            {
                NetIncomingMessage im = client.ReadMessage();
                if (im == null)
                    break;

                switch (im.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        GameSystem.LogSystem.WriteLine("Received Data");
                        PacketBase pb = PacketSerializer.Deserialize(im.ReadString());
                        if (pb is SyncPacket)
                        {
                            SyncPacket sp = pb as SyncPacket;
                            boardView.BoardData = sp.BlockData;
                            boardView.Score = sp.Score;
                            EnemyGameOvered = sp.IsGameOver;
                        }
                        else
                            throw new ArgumentException();
                        break;
                }
            } while (true);

            if (client.ConnectionStatus != NetConnectionStatus.Disconnected)
            {
                board.Update(deltaTime);

                NetOutgoingMessage msg = client.CreateMessage(
                    PacketSerializer.Serialize(new SyncPacket(board.GetViewData(), board.Score, board.GameOvered)));
                client.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);
                client.FlushSendQueue();
            }

            if (board.GameOvered == true && EnemyGameOvered == true)
            {
                if (board.Score > boardView.Score)
                    WinLoseLabel.Text = "Win";
                else if (board.Score < boardView.Score)
                    WinLoseLabel.Text = "Lose";
                else
                    WinLoseLabel.Text = "Draw";
            }
            if (InputHelper.IsPressed(Key.Escape) && board.GameOvered == true)
            {
                GameSystem.RunGameLoop(new MainBoardView());
            }
        }

        public override void Render(Second deltaTime)
        {
            BackgroundSprite.Render(ref ocam.Matrix);
            DescriptionSprite.Render(ref ocam.Matrix);
            board.Render(ref ocam.Matrix);
            boardView.Render(ref ocam.Matrix);

            if (board.GameOvered == true && EnemyGameOvered == true)
            {
                WinLoseLabel.Render(ref ocam.Matrix);
            }
            else if (board.GameOvered == true)
            {
                PlayerGameOverSprite.Render(ref ocam.Matrix);
            }
            else if (EnemyGameOvered == true)
            {
                EnemyGameOverSprite.Render(ref ocam.Matrix);
            }
        }
    }
}
