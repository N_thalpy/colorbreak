﻿using Colorbreak.GameObject;

namespace Colorbreak.Network.Packet
{
    public class SyncPacket : PacketBase
    {
        public Block[,] BlockData;
        public int Score;
        public bool IsGameOver;

        public SyncPacket()
        {
        }
        public SyncPacket(Block[,] blockData, int score, bool isGameOver)
        {
            BlockData = blockData;
            Score = score;
            IsGameOver = isGameOver;
        }
    }
}
