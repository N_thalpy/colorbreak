﻿using OpenTK;
using System.Collections.Generic;
using System.Drawing;
using Tri.Win.UI.BitmapFont;
using Tri.Win.Rendering;
using Tri.Win.UI;

namespace Colorbreak.GameObject
{
    public class GameBoardView
    {
        private static Dictionary<Block, Texture> TextureDic;
        private static Texture UITexture;
        private static float TileSize;

        public int Width;
        public int Height;

        public Vector2 Position;
        private Vector2 size;

        private Sprite sp;

        public int Score;
        private BitmapLabel scoreText;

        public Block[,] BoardData;

        static GameBoardView()
        {
            TileSize = 50;

            UITexture = Texture.CreateFromBitmapPath("Resources/UI.png");
            TextureDic = new Dictionary<Block, Texture>();
            TextureDic.Add(Block.None, Texture.CreateFromBitmapPath("Resources/cell_none.png"));
            TextureDic.Add(Block.Void, Texture.CreateFromBitmapPath("Resources/cell_Void.png"));
            TextureDic.Add(Block.R, Texture.CreateFromBitmapPath("Resources/cell_R.png"));
            TextureDic.Add(Block.G, Texture.CreateFromBitmapPath("Resources/cell_G.png"));
            TextureDic.Add(Block.B, Texture.CreateFromBitmapPath("Resources/cell_B.png"));
            TextureDic.Add(Block.RG, Texture.CreateFromBitmapPath("Resources/cell_RG.png"));
            TextureDic.Add(Block.RB, Texture.CreateFromBitmapPath("Resources/cell_RB.png"));
            TextureDic.Add(Block.GB, Texture.CreateFromBitmapPath("Resources/cell_GB.png"));
            TextureDic.Add(Block.RGB, Texture.CreateFromBitmapPath("Resources/cell_RGB.png"));
        }
        public GameBoardView(int width, int height)
        {
            Width = width;
            Height = height;
            size = new Vector2(width, height) * TileSize;
            BoardData = new Block[width, height];

            sp = new Sprite()
            {
                Color = Color.White,
                Texture = null,
                Size = Vector2.One * TileSize,
                Position = Vector2.Zero,
                Anchor = Vector2.Zero,
            };


            scoreText = new BitmapLabel()
            {
                FontSize = 50,
                BitmapFont = BitmapFont.Create("Resources/Gamegirl.fnt"),
                Position = Vector2.Zero,
                Anchor = Vector2.One * 0.0f,
                Color = Color.White,
                Text = "000000000000000",
            };
        }

        public void SetBoardData(Block[,] board, int score)
        {
            BoardData = (Block[,])board.Clone();
            Score = score;
        }
        public void Render(ref Matrix4 mat)
        {
            Vector2 leftdown = Position - size * 0.5f;

            sp.Anchor = Vector2.Zero;
            sp.Size = Vector2.One * TileSize;
            // Render board first.
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                {
                    sp.Position = new Vector2(x, y) * TileSize + leftdown;
                    sp.Texture = TextureDic[BoardData[x, y]];
                    sp.Render(ref mat);
                }
            
            // Render falli
            // Render UI last.
            sp.Size = UITexture.Size;
            sp.Anchor = Vector2.One * 0.5f;
            sp.Position = Position;
            sp.Texture = UITexture;
            sp.Render(ref mat);

            scoreText.Text = Score.ToString();
            scoreText.Position = leftdown + Vector2.UnitY * Height * TileSize + new Vector2(-20, 10);
            scoreText.Render(ref mat);
        }
    }
}
